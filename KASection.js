
/*!
 * KAS_Sections: 1.2
 * http://www.klipartstudio.com/
 *
 * Copyright 2012, Klip Art Studio, LLC
 *
 * DEPENDENCIES: Greensock & Jquery
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 * 	 this		  : Targted Scope
 *   name         : Div ID name you would like to target
 *   target	      : Main Div ID you would like to target String or Object
 *   file	      : The Json file used to develop the code and animation
 *   onCopmplete  : Call a function once it is complete String or Medthod
 *  
 * USAGE:
 *   var divTarget = document.getElementById("divTarget");
 * 
 *   function finished(param) {
 *      console.log('LOAD COMPLETE', param);
 * 		stage1.startSection();
 *   }

 *   var stage1 = new kas.Section();
 * 	 stage1.Tween(this, {
				name:"myDiv", 
				target:divTarget, 
				file:"main.json", 
				onComplete:finished
			});
 * 	
 * Availible options for animation
 *	stage1.timeCall('timeline Name', 'jump', (INT seconds));
 * 	stage1.timeCall('timeline Name', 'reverse', '');
 * 	stage1.timeCall('timeline Name', 'hide', '');
 * 	stage1.timeCall('timeline Name', 'to', 'label name');
 * 	stage1.timeCall('timeline Name', 'play', '');
 * 
 * HTML Needed
 *       <div id='divTarget' class='main'></div>
 *
 * AUTHOR: Jose Fonseca, josef@klipartstudio.com
 * DATE: 11/11/2011
 */




var kas = {};
kas.Section = function() {
	this.base = this;
	this.section_data = new Object();
	this.section_id  = '';
	this.section     = '';
	this.menu = false;
	this.p_obj 	     = [];
	this.section_txt = [];
	this.section_nav = [];
	this.section_img = [];
	this.section_vid = [];
	this.section_mc  = [];
	this.section_menu  = [];
	this.timelines = [];
	this.masterList = [];
	this.activeTL;
	this.animationTL = [];
	this.skip_load  = false;
	this.state  	= 'close';
	this.selective  = false;
	
}
kas.Section.prototype.Tween = function(scope, p_parameters) {
	//this.animate(selector);
	this.scope = scope;
	this.section_id = p_parameters.name;
	this.p_obj = p_parameters;
	this.load(p_parameters.file, p_parameters);
}

kas.Section.prototype.load = function(loadData, p_parameters) {
	var dataLoaded;
	var base = this;
	//this.masterList[p_parameters.name] = p_parameters;
	$.getJSON(loadData, function(data) {
		base.section_data = data;
		p_parameters.data = data;
		base.build(p_parameters);

		if(typeof(p_parameters.onComplete)=='function'){
			p_parameters.onComplete();
		} else if(typeof(p_parameters.onComplete)=='string'){
			window[p_parameters.onComplete](p_parameters);
		}
	})
}
//BUILDOUT ANIMATION
kas.Section.prototype.build = function(p_parameters){
	/*console.log('BUILD >>>>>>>>>>>>' + p_parameters.data.sections[0].content.length);*/
	var count = p_parameters.data.sections[0].content.length;
	var contentData = p_parameters.data.sections[0].content;
	for(var i = 0; i < count; i++) {
	
		/*console.log('BUILD mcs  >>>>>>>>>>>>' + contentData[i].mcs);*/
		var mcs = (contentData[i].mcs) ? this.MovieClips(contentData[i].mcs, p_parameters) : '';
		
		/*console.log('BUILD imgs  >>>>>>>>>>>>' + contentData[i].imgs);*/
		var imgs = (contentData[i].imgs) ? this.Images(contentData[i].imgs, p_parameters) : '';
		
		/*console.log('BUILD txts  >>>>>>>>>>>>' + contentData[i].txts);*/
		var txts = (contentData[i].txts) ? this.Txts(contentData[i].txts, p_parameters) : '';
		
		/*console.log('BUILD menu  >>>>>>>>>>>>' + contentData[i].menu);*/
		var menu = (contentData[i].menu) ? this.Menu(contentData[i].menu, p_parameters) : '';
		
		/*console.log('BUILD timeline  >>>>>>>>>>>>' + contentData[i].timeline);*/
		var timeline = (contentData[i].timeline) ? this.Timeline(contentData[i].timeline, p_parameters) : '';
	}
	
	p_parameters.mcs = mcs;
	p_parameters.imgs = imgs;
	p_parameters.menu = menu;
	p_parameters.txts = txts;
	this.baseTimeLine('MC_TIMELINE',this.section_mc);
	this.baseTimeLine('IMGS_TIMELINE',this.section_img);
	this.baseTimeLine('TXT_TIMELINE',this.section_txt);
	this.baseTimeLine('MENU_TIMELINE',this.section_menu);
	this.masterList[p_parameters.name] = p_parameters;

}
kas.Section.prototype.baseTimeLine = function(nameToUse, items)  {

		var tl = new TimelineLite({align:"normal"});
		var objTemp = [];
		for(i = 0; i < items.length; i++) { 
			objTemp.push(this.buildAnimation(items[i], items[i].params));
			
		}
		tl.add(objTemp);
		tl.stop();
		var objTemp = new Object();
		objTemp.id = nameToUse;
		objTemp.obj = tl;
		objTemp.arr = items;
		this.timelines[nameToUse] = objTemp;
		
}
/**
 * Custom Array Build
 *
 * @return
 */
kas.Section.prototype.MovieClips = function(items, p_parameters)  {
	/*console.log("MovieClips Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items.length);*/
	var targetItem;
	var count = items.length;
	var mcs = [];
	for(var i = 0; i < count; i++) {
		
		targetItem = this.createdivNew(items[i], p_parameters);
		
		if(items[i].active){
			this.addClick(targetItem);
		}
		
		if(items[i].classId){ this.addClass(targetItem, items[i].classId);}
		
		if(items[i].imgs !== undefined){
			/*console.log("IMAGES FROM MC Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items[i].imgs.length);*/
			var paramsTemp = [];
			paramsTemp.target = targetItem;
			this.Images(items[i].imgs, paramsTemp);
		}
		if(items[i].txts !== undefined){
			/*console.log("TEXTS FROM MC Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items[i].txts);*/
			var paramsTemp = [];
			paramsTemp.target = targetItem;
			this.Txts(items[i].txts, paramsTemp);
		}
		
		this.section_mc.push(targetItem);
		mcs.push(targetItem)
	}
	
	return mcs;
}
kas.Section.prototype.Images = function(items, target) {
	/*console.log("Images Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items.length);*/
	
	var targetItem;
	var count = items.length;
	var mcs = [];
	for(var i = 0; i < count; i++) {

		/*console.log("items[i].image Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items[i].image);*/
		targetItem = this.createimg(items[i], target);
		if(items[i].active){
			this.addClick(targetItem);
		}
		if(items[i].classId){
			this.addClass(targetItem, items[i].classId);
		}
		this.setDefaults(targetItem, items[i]);
		this.section_img.push(targetItem);
		mcs.push(targetItem)
	}
	
	return mcs;
}
kas.Section.prototype.Txts = function(items, p_parameters)  {
	/*console.log("MovieClips Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items.length);*/
	var targetItem;
	var count = items.length;
	var mcs = [];
	for(var i = 0; i < count; i++) {
		
		targetItem = this.createdivNew(items[i], p_parameters);
		if(items[i].active){
			this.addClick(targetItem);
		}
		targetItem.style.position = 'absolute';
		
		if(items[i].classId){
			this.addClass(targetItem, items[i].classId);
		}
		if(items[i].shadow){
			TweenMax.to(targetItem, 0.2, {textShadow:items[i].shadow});
		}
		this.setDefaults(targetItem, items[i]);
		this.section_txt.push(targetItem);
		mcs.push(targetItem)
	}
	
	return mcs;
}
kas.Section.prototype.Menu = function(items, target) {
	/*console.log("Images Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items.length);*/
	
	var targetItem;
	var count = items.length;
	var mcs = [];
	for(var i = 0; i < count; i++) {
		
		/*console.log("items[i].image Length >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + items[i].image);*/
		targetItem = this.createimg(items[i], target);
		if(items[i].active){
			this.addClick(targetItem);
		}
		if(items[i].classId){
			this.addClass(targetItem, items[i].classId);
		}
		this.setDefaults(targetItem, items[i]);
		this.section_menu.push(targetItem);
		mcs.push(targetItem)
	}
	
	return mcs;
}
kas.Section.prototype.globalStart = function(params)  {
//onComplete:myMC.gotoAndStop, onCompleteParams:[1]});
	//params = params[0];
	for (var prop in params) {};//console.log('globalStart  >>  ' + prop + " = " + params[prop]);}
}
kas.Section.prototype.globalComplete = function(params)  {
	//params = params[0];
	for (var prop in params) {};//console.log('globalComplete  >>  ' + prop + " = " + params[prop]);}
	
}
kas.Section.prototype.Timeline = function(ani, _params)  {
	var countBase = ani.length;
	
	
	for(var b = 0; b < countBase; b++) {
		var mcs;
		var timeline = ani[b].seq;
		var count = timeline.length;
		var tweenUsed = [];
		var objectsUsed = [];
		
		var alignTemp = 'normal';
		if(ani[b].align !== undefined){alignTemp = ani[b].align}
        var timePosition = "0";
		if(ani[b].timePosition !== undefined){timePosition = ani[b].timePosition}
		var staggerAmt = 0.0;
		if(ani[b].staggerAmt !== undefined){staggerAmt = ani[b].staggerAmt}
		var onComplete;
		if(ani[b]['onComplete'] !== undefined){
			//console.log('onCompleteUse  IS NOT UNDEFINED  >>>>>>>>>>>>>>>>>>>>>)))))))))))))))))))>>>>>>>>>>>>>>>>>>>>>>>>>> ' + ani[b]['onComplete']);
		   	if(typeof(ani[b]['onComplete'])=='function'){
		   		//console.log('---- onCompleteUse  IS FUNCTION  >>>>>>>>>>>>>>>>>>>>>)))))))))))))))))))>>>>>>>>>>>>>>>>>>>>>>>>>> ' + ani[b]['onComplete']);
				onComplete = ani[b]['onComplete'];
			} else if(typeof(ani[b]['onComplete'])=='string'){
				//console.log('---- onCompleteUse  IS STRING  >>>>>>>>>>>>>>>>>>>>>)))))))))))))))))))>>>>>>>>>>>>>>>>>>>>>>>>>> ' + ani[b]['onComplete']);
				onComplete = window[ani[b]['onComplete']];
			}
	   		
	   }
		//{align:"normal"}
		
		for(var i = 0; i < count; i++) {
			var itemSel = timeline[i];
			
			if(itemSel.items){
				//IF THERE IS MUTIPLES THAT WILL DO SAME ANIMATION
				this.TimelineSeq(itemSel, tl);
			} else {
				
			
				//SINGLE BUILD OUTS
				var objReturn = this.TimelineSingle(itemSel, tl);
				
				var tweenCount = objReturn.tw.length;
				for(var t = 0; t < tweenCount; t++) {
					tweenUsed.push(objReturn.tw[t]);
				}
				
				//console.log(i + " ONE ITEM  " + objReturn.tw + '   LENGTH : ' + objReturn.tw.length);
				objectsUsed.push(objReturn);

			}
		}
		var tl;
		
		//console.log('REPEAT   ' + ani[b].repeat);
		var onCompleteUse = '';
		//console.log('onCompleteUse  >>>>>>>>>>>>>>>>>>>>>)))))))))))))))))))>>>>>>>>>>>>>>>>>>>>>>>>>> ' + ani[b].id);

		//console.log('onCompleteUse  '+b+' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>)))))))))))))))))))))))))))))))))))))))))))))))))))>>>>>>>>>>>>>>>>>>>>>>>>>>>> ' + typeof(onComplete));

		
		if(ani[b].repeat !== undefined){
			
			tl = new TimelineMax({repeat:-1, onComplete:onComplete, onCompleteParams:[ani[b]]});
			if(ani[b].repeat !== undefined){}
		} else {
			
			tl = new TimelineLite({onComplete:onComplete, onCompleteParams:[ani[b]]});
		}
		
		if(tweenUsed.length !== 0){
			var objCollect = [];
			tl.add(tweenUsed, timePosition, alignTemp, staggerAmt);
		}
		this.animationTL.push(tl);
		tl.stop();
		
		var objTemp = new Object();
		objTemp.id = ani[b].id;
		objTemp.obj = tl;
		objTemp.arr = objectsUsed;
		//console.log(tweenUsed.length + " <<<<<<<< timelines ID : " +objTemp.id);
		this.timelines[objTemp.id] = objTemp;
	}
	
	return mcs;
}
/**
 * Custom Array Build
 *
 * @return
 */
kas.Section.prototype.TimelineSeq = function(itemSel, tl)  {

	var itemsCount  = itemSel.items.length;
	var _items 		= itemSel.items;
	var timeArray 	= [];
	var mainObj 	= document.createElement('div'); 
	
/*
	mainObj.setAttribute('id', itemSel.id); 
	mainObj.params = itemSel;
*/

	this.setDefaults(mainObj, itemSel);
	mainObj.style.position = "relative";
	
	for(var j = 0; j < itemsCount; j++) {
		var innerObj = this.BuildSeq(innerObj, _items[j]);
		
		//innerObj.params = _items[j];
		this.setDefaults(innerObj, _items[j]);
		
		timeArray.push(innerObj);
		mainObj.appendChild(innerObj);
		//itemSel.params = this.setParams(itemSel);
		
	}

	tl.staggerTo(timeArray, itemSel.duration, innerObj.params, itemSel.stagger);
		
	var targetDiv;
	if(typeof(this.p_obj.target)=='object') {
		targetItem = this.p_obj.target;
	} 
	else {
		targetItem = document.getElementById(this.p_obj.target);
	}
	
	
	targetItem.appendChild(mainObj);
}
kas.Section.prototype.TimelineSingle = function(itemSel)  {
	var innerObj = this.BuildSeq(innerObj, itemSel);
	var objTemp = [];
	var targetDiv;
	if(typeof(this.p_obj.target)=='object') {
		targetItem = this.p_obj.target;
	} 
	else {
		targetItem = document.getElementById(this.p_obj.target);
	}
	this.setDefaults(innerObj, itemSel);
	//
	if(itemSel.open !== undefined || itemSel.close !== undefined){
		if(itemSel.open !== undefined){
			//var sub = new TimelineLite();
			
			var delayOffset = 0;
			if(itemSel.delay !== undefined){
				delayOffset = itemSel.delay;
			} else if(itemSel.delay == undefined){
				itemSel.delay = 0;
			}
			
			var itemsCount  = itemSel.open.length;
			for(var j = 0; j < itemsCount; j++) {
				
				//console.log('delayOffset  ' + delayOffset);
				//console.log('itemSel.open[j].delay  ' + itemSel.open[j].delay);
				if(itemSel.open[j].delay  == undefined){
					itemSel.open[j].delay = delayOffset;
				} else {
					itemSel.open[j].delay = itemSel.open[j].delay + delayOffset;
				}
				
				//console.log('AFTER  itemSel.open[j].delay  ' + itemSel.open[j].delay);
				
				objTemp.push(this.buildAnimation(innerObj, itemSel.open[j]));
				//sub.add(this.buildAnimation(innerObj, itemSel.open[j]));
			}
			innerObj.style.position = "absolute";
			//sub.add(objTemp);
			//sub.stop();
			//objTemp.push(sub);
			
			//tl.add(sub);
		}
		
	} else {
		objTemp.push(this.buildAnimation(innerObj, itemSel));
		//tl.add(this.buildAnimation(innerObj, itemSel));
	}
	
	
	targetItem.appendChild(innerObj);
	innerObj.tw = objTemp;
	return innerObj;
}

/*
	this.section_data = loadData;
	console.log(loadData.sections[0].content[0].mcs[0]);
	this.build();
	if(this.masterList[this.section_id].onComplete){
		this.masterList[this.section_id].onComplete();
	}
*/
kas.Section.prototype.createimg = function(file, p_parameters) {

		var img = new Image();
		img.src = file.image;
		img.onload = function() {
		    //document.body.appendChild( img );
		}
		
		//img.setAttribute('id', file.id); 
		var targetDiv;
		if(typeof(p_parameters.target)=='object') {
			targetItem = p_parameters.target;
		} 
		else {
			targetItem = document.getElementById(p_parameters.target);
		}
		var image_item = targetItem.appendChild(img);
		this.setDefaults(img, file);
		image_item.style.position = "absolute"; 

        /*console.log('file.ypos   >>>>>>>>>>>>>>>>>>>>>>>>> ' + file.ypos );*/
		/*console.log('image_item   >>>>>>>>>>>>>>>>>>>>>>>>> ', image_item );*/
		
	return image_item;
}

kas.Section.prototype.createdivNew = function(obj, p_parameters) {

	var newdiv = document.createElement('div'); 
	//newdiv.setAttribute('id', obj.id); 

	if (obj.info) { newdiv.innerHTML = obj.info; } else { newdiv.innerHTML = ""; } 

	var targetDiv;
	if(typeof(p_parameters.target)=='object') {
		targetItem = p_parameters.target;
	} 
	else {
		targetItem = document.getElementById(p_parameters.target);
	}
	newdiv.style.position = "absolute";
	this.setDefaults(newdiv, obj);
	var image_item = targetItem.appendChild(newdiv);
	
	
	
	return image_item;
}
//ADD MOUSE BUTTON
/*
function hasClass(el, name) {
   return new RegExp('(\\s|^)'+name+'(\\s|$)').test(el.className);
}
function addClass(el, name)
{
   if (!hasClass(el, name)) { el.className += (el.className ? ' ' : '') +name; }
}
function removeClass(el, name)
{
   if (hasClass(el, name)) {
      el.className=el.className.replace(new RegExp('(\\s|^)'+name+'(\\s|$)'),' ').replace(/^\s+|\s+$/g, '');
   }
}
*/
kas.Section.prototype.addClick = function(thisObj){
	if(thisObj.addEventListener)
	{
	  thisObj.addEventListener("mouseover", this.onMouseOver, false);
	  thisObj.addEventListener("mouseout", this.onMouseOut, false);
	  thisObj.addEventListener('mouseup', this.onMouseUp, false);
	} else {
		thisObj.attachEvent("onmouseover", this.onMouseOver, false);
		thisObj.attachEvent("onmouseout", this.onMouseOut, false);
		thisObj.attachEvent('onmouseup', this.onMouseUp, false);
	}
	thisObj.style.cursor = 'pointer';
}
kas.Section.prototype.onMouseOver = function(event) {
	event = event || window.event;
	var tempParms = event.target.params;

	if(tempParms.on !== undefined){
		var obj = tempParms.on;
		if(obj[0] !== undefined){
			obj = obj[0];
		}
		if(obj['image']){
			event.target.src = obj['image'];
		}
		
		tempParms.base.buildAnimation(event.target, tempParms.on);
	}	
}
kas.Section.prototype.onMouseOut = function(event) {
	var tempParms = event.target.params;
	if(tempParms.off !== undefined && event.target.style.opacity == 1){
		var obj = tempParms.off;
		if(obj[0] !== undefined){
			obj = obj[0];
		}
		if(obj['image']){
			event.target.src = obj['image'];
		}
		tempParms.base.buildAnimation(event.target, tempParms.off);
	}
		
}
kas.Section.prototype.onMouseUp = function(event) {
		/*console.log('onMouseUp  >>>>   ' + event.target.className);*/
		/*console.log('activeClick  >>>>   ' + event.target.active);*/
		/*console.log('activeClick  >>>>   ' + event.target.jump);*/
		var startDiv, targetItem;
		var tempParms = event.target.params;
		eval(tempParms.jump)(event);
		if(typeof(tempParms.jump)=='function'){
			tempParms.jump(event);
		} else if(typeof(event.target.jump)=='string'){
			window[tempParms.jump](event);
		}

}


/**
 * Custom Array setParams
 *
 * @return
 */
kas.Section.prototype.setDefaults = function(obj, params){
	if(params[0] !== undefined){
		params = params[0];
	}
	
	var tweenBuild  = [];

	//tweenBuild.display = "block";
	   /*console.log("((((((((((((((((((((((((((((((((((((((((((((((((START DEFAULT )))))))))))))))))))))))))))))))))))))))))))))))))" );*/
   for (var prop in params) {
   		/*console.log(prop + " = " + params[prop]);*/
	   if(prop == 'top'){
		   tweenBuild.top=params[prop];
	   }else if(prop == 'left'){
		   tweenBuild.left=params[prop];
	   }else if(prop == 'x'){
		   tweenBuild.x=params[prop];
	   }else if(prop == 'y'){
		   tweenBuild.y=params[prop];
	   }else if(prop == 'autoAlpha'){
		   tweenBuild.autoAlpha=params[prop];
		   tweenBuild.opacity=params[prop];
	   }else if(prop == 'rotation'){
	   	   tweenBuild.rotation=params[prop];
	   }else if(prop == 'rotationX'){
	   	   tweenBuild.rotationX=params[prop];
	   }else if(prop == 'rotationY'){
	   	   tweenBuild.rotationY=params[prop];
	   }else if(prop == 'scale'){
	   	   tweenBuild.scale=params[prop];
	   }else if(prop == 'scaleX'){
	   	   tweenBuild.scale=params[prop];
	   }else if(prop == 'scaleY'){
	   	   tweenBuild.scale=params[prop];
	   }else if(prop == 'skewX'){
	   		tweenBuild.skewX=params[prop];
	   }else if(prop == 'skewY'){
	   		tweenBuild.skewY=params[prop];
	   }else if(prop == 'backgroundColor'){
	   		tweenBuild.backgroundColor=params[prop];
	   }else if(prop == 'color'){
	   		tweenBuild.color=params[prop];
	   }else if(prop == 'width'){
	   		tweenBuild.width=params[prop];
	   }else if(prop == 'height'){
	   		tweenBuild.height=params[prop];
	   }else if(prop == 'overwrite'){
	   		tweenBuild.overwrite=params[prop];
	   }else if(prop == 'delay'){
	   		tweenBuild.delay=params[prop];
	   }else if(prop == 'ease'){
	   		tweenBuild.ease=params[prop];
	   }else if(prop == 'display'){
	   		tweenBuild.display=params[prop];
	   }else if(prop == 'zindex'){
	   		tweenBuild.zIndex=params[prop];
	   }else if(prop == 'borderRadius'){
	   		tweenBuild.borderRadius=params[prop];
	   }else if(prop == 'boxShadow'){
	   		tweenBuild.boxShadow=params[prop];
	   }else if(prop == 'border'){
	   		tweenBuild.border=params[prop];
	   }else if(prop == 'padding'){
	   		tweenBuild.padding=params[prop];
	   }else if(prop == 'fontSize'){
	   		tweenBuild.fontSize=params[prop];
	   }else if(prop == 'fontWeight'){
	   		tweenBuild.fontWeight=params[prop];
	   }else if(prop == 'fontAlign'){
	   		tweenBuild.textAlign=params[prop];
	   }else if(prop == 'lineHeight'){
	   		tweenBuild.lineHeight=params[prop];
	   }else if(prop == 'clip'){
	   		tweenBuild.clip=params[prop];
	   }else if(prop == 'onComplete'){
		   	if(typeof(params[prop])=='function'){
				tweenBuild.onComplete= params[prop];
			} else if(typeof(params[prop])=='string'){
				tweenBuild.onComplete= window[params[prop]]();
			}
	   		
	   }
	   
   }
   tweenBuild.base = this;
   if(tweenBuild.autoAlpha == undefined) {tweenBuild.autoAlpha = 1;tweenBuild.opacity=1;}
   if(tweenBuild.display == undefined) {tweenBuild.display = 'block';}
   /*console.log("autoAlpha = " + tweenBuild.autoAlpha);*/
   /*console.log("display = " + tweenBuild.display);*/
   
   	var initBuild  = [];
   	
   	
   	//params[0] !== undefined
   	   if(params.startTop !== undefined){ 
	   		initBuild.top=params.startTop;
	   } else if(params.top !== undefined) {
	       initBuild.top=params.top;
	   } else if(params.y !== undefined) {
	       initBuild.top=params.y;
	   }
   	   
	   if(params.startLeft !== undefined){ 
	   		initBuild.left=params.startLeft;
	   } else if(params.left !== undefined) {
	       initBuild.left=params.left;
	   } else if(params.x !== undefined) {
	       initBuild.left=params.x;
	   }
	   
	   if(params.startAlpha !== undefined){ initBuild.autoAlpha=params.startAlpha;initBuild.opacity=params.startAlpha;} else { initBuild.autoAlpha=0; initBuild.opacity=0;}
	   if(params.startRotation !== undefined){ initBuild.rotation=params.startRotation;}
	   if(params.startRotationX !== undefined){ initBuild.rotationX=params.startRotationX;}
	   if(params.startRotationY !== undefined){ initBuild.rotationY=params.startRotationY;}
	   if(params.startScale !== undefined){ initBuild.scale=params.startScale;}
	   if(params.startScaleX !== undefined){ initBuild.scaleX=params.startScaleX;}
	   if(params.startScaleY !== undefined){ initBuild.scaleY=params.startScaleY;}
	   if(params.startSkewX !== undefined){ initBuild.skewX=params.startSkewX;}
	   if(params.startSkewY !== undefined){ initBuild.skewY=params.startSkewY;}
	   if(params.startBackgroundColor !== undefined){ initBuild.backgroundColor=params.startBackgroundColor;} else { initBuild.backgroundColor="transparent"; }
	   if(params.startColor !== undefined){ initBuild.color=params.startColor;}
	   if(params.startWidth !== undefined){ initBuild.width=params.startWidth;} else {initBuild.width=params.width;}
	   if(params.startHeight !== undefined){ initBuild.height=params.startHeight;} else {initBuild.height=params.height;}
	   if(params.startDisplay !== undefined){ initBuild.display=params.startDisplay;} else {initBuild.display='block';}
	   if(params.startZIndex !== undefined){ initBuild.zIndex=params.startZIndex;}
	   if(params.startBoxShadow !== undefined){ initBuild.boxShadow=params.startBoxShadow;}
	   if(params.startClip !== undefined){ initBuild.clip=params.startClip;}	   
	   
	   
	   obj.setAttribute('id', params.id); 
	   
	   if(params.startFontSize !== undefined){ initBuild.fontSize=params.startFontSize;} else if(params.fontSize !== undefined){initBuild.fontSize=params.fontSize;}
	   if(params.startFontWeight !== undefined){ initBuild.fontWeight=params.startFontWeight;}else if(params.fontWeight !== undefined){initBuild.fontWeight=params.fontWeight;}

	   TweenLite.set(obj, initBuild);
	   params.base = this;
	   obj.params = params;
	   /*console.log("((((((((((((((((((((((((((((((((((((((((((((((((SECOND START DEFAULT )))))))))))))))))))))))))))))))))))))))))))))))))" );*/
	   for (var prop in params) {
	   		/*console.log(prop + " = " + params[prop]);*/
	   }
	   
	   /*console.log("((((((((((((((((((((((((((((((((((((((((((((((((END DEFAULT )))))))))))))))))))))))))))))))))))))))))))))))))" );*/
	   return tweenBuild;

}
kas.Section.prototype.BuildSeq = function(innerObj, _params){

	for (var prop in _params) {
		console.log('BuildSeq  ....?>>>> ' +prop + " = " + _params[prop]);
	}
	if(_params.itemType === 'txt')
	{
		innerObj = document.createElement('div'); 
		innerObj.innerHTML = _params.info;
	} else if(_params.itemType === 'img')
	{
		innerObj = new Image();
		innerObj.src = _params.image;
		innerObj.onload = function() {
		    //document.body.appendChild( img );
		}
		
	} else if(_params.itemType === 'mc'){
		innerObj = document.createElement('div');
	} else if(_params.itemType === 'id'){
		innerObj = document.getElementById(_params.id);
	}
	
	
	if(_params.classId){
		this.addClass(innerObj, _params.classId);
	}
	innerObj.style.position = "absolute";
	
	this.setDefaults(innerObj, _params);
	if(_params.active){
/*
		innerObj['section'] = _params.section;
		innerObj['active']  = _params.active;
		innerObj['jump']    = _params.jump;
*/
		this.addClick(innerObj);
	}
	return innerObj;
}
kas.Section.prototype.buildAnimation = function(item, params) {
	if(params[0] !== undefined){
		params = params[0];
	}
	
	var tweenBuild  = [];

	//tweenBuild.display = "block";
	var tweenBuild  = [];

	//tweenBuild.display = "block";
	/*console.log("-------------------------START BUILD ANIMATIONS---------------------------------------------------------------------------" );*/
   for (var prop in params) {
   		/*console.log(prop + " = " + params[prop]);*/
	   if(prop == 'top'){
		   tweenBuild.top=params[prop];
	   }else if(prop == 'left'){
		   tweenBuild.left=params[prop];
	   }else if(prop == 'x'){
		   tweenBuild.x=params[prop];
	   }else if(prop == 'y'){
		   tweenBuild.y=params[prop];
	   }else if(prop == 'autoAlpha'){
		   tweenBuild.autoAlpha=params[prop];
	   }else if(prop == 'rotation'){
	   	   tweenBuild.rotation=params[prop];
	   }else if(prop == 'rotationX'){
	   	   tweenBuild.rotation=params[prop];
	   }else if(prop == 'rotationY'){
	   	   tweenBuild.rotation=params[prop];
	   }else if(prop == 'scale'){
	   	   tweenBuild.scale=params[prop];
	   }else if(prop == 'scaleX'){
	   	   tweenBuild.scale=params[prop];
	   }else if(prop == 'scaleY'){
	   	   tweenBuild.scale=params[prop];
	   }else if(prop == 'skewX'){
	   		tweenBuild.skewX=params[prop];
	   }else if(prop == 'skewY'){
	   		tweenBuild.skewY=params[prop];
	   }else if(prop == 'backgroundColor'){
	   		tweenBuild.backgroundColor=params[prop];
	   }else if(prop == 'color'){
	   		tweenBuild.color=params[prop];
	   }else if(prop == 'width'){
	   		tweenBuild.width=params[prop];
	   }else if(prop == 'height'){
	   		tweenBuild.height=params[prop];
	   }else if(prop == 'overwrite'){
	   		tweenBuild.overwrite=params[prop];
	   }else if(prop == 'delay'){
	   		tweenBuild.delay=params[prop];
	   }else if(prop == 'ease'){
	   		tweenBuild.ease=params[prop];
	   }else if(prop == 'display'){
	   		tweenBuild.display=params[prop];
	   }else if(prop == 'fontSize'){
	   		tweenBuild.fontSize=params[prop];
	   }else if(prop == 'borderRadius'){
	   		tweenBuild.borderRadius=params[prop];
	   }else if(prop == 'boxShadow'){
	   		tweenBuild.boxShadow=params[prop];
	   }else if(prop == 'padding'){
	   		tweenBuild.padding=params[prop];
	   }else if(prop == 'border'){
	   		tweenBuild.border=params[prop];
	   }else if(prop == 'fontWeight'){
	   		tweenBuild.fontWeight=params[prop];
	   }else if(prop == 'fontAlign'){
	   		tweenBuild.textAlign=params[prop];
	   }else if(prop == 'lineHeight'){
	   		tweenBuild.lineHeight=params[prop];
	   }else if(prop == 'clip'){
	   		tweenBuild.clip=params[prop];
	   }else if(prop == 'onComplete'){
		   	if(typeof(params[prop])=='function'){
				tweenBuild.onComplete= params[prop];
			} else if(typeof(params[prop])=='string'){
				tweenBuild.onComplete= window[params[prop]]();
			}
	   		
	   }
   }
    if(tweenBuild.autoAlpha == undefined) {tweenBuild.autoAlpha = 1;tweenBuild.opacity = 1;}
   /*console.log("-------------------------END BUILD ANIMATIONS---------------------------------------------------------------------------" );*/
   return TweenLite.to(item, params.duration, tweenBuild);


}

kas.Section.prototype.findById = function (source, id) {
  for (var i = 0; i < source.length; i++) {
    if (source[i].id === id) {
      return source[i];
    }
  }
  throw "Couldn't find object with id: " + id;
}
kas.Section.prototype.lookdeep = function(obj){
    var A= [], tem;
    for(var p in obj){
    	if(obj.hasOwnProperty(p)){
    		tem= obj[p];
    		if(tem && typeof tem=='object'){
    			A[A.length]= p+':{ '+arguments.callee(tem).join(', ')+'}';
    		}
    		else A[A.length]= [p+':'+tem.toString()];
    	}
    }
    return A;
}
/**
 * Custom Array Build
 *
 * @return
 */
kas.Section.prototype.timeCall = function(timelineId, state, label) {
	console.log(typeof(this.timelines[timelineId]) + "     timeCall timeCall >>>>>>>>>>>>>>>> "+timelineId+"  >>>>>>>>>>>>>>>>>>>>>>>>>!!!!!!!!!!!!!!!!!!!!!!!!!!!!   " + this.timelines[0] + "!!!!!!!!!!!!!!!!!!!!!   STATE = " + state  );
   for (var prop in this.timelines[timelineId]) {
      console.log(prop + " = " + this.timelines[timelineId][prop]);
   }
   for (var prop in this.timelines['MC_TIMELINE']) {
      console.log('MC_TIMELINE  >>  ' +prop + " = " + this.timelines['MC_TIMELINE'][prop]);
   }
   var TL;
   if(timelineId != ''){
	   TL = this.timelines[timelineId].obj;
   }
	
	
/* 		TL.timeScale(0.2); */
//	TL.play();
/*
	var TL2 = this.timelines['IMGS_TIMELINE'].obj;
	TL2.timeScale(0.2);
	TL2.play();
*/
	
	if("reverse" == state){
		TL.timeScale(2.5);
		TL[state]();
	} else if("hide" == state){
		TL.seek(0);
		TL.stop();
	}else if("jump" == state){
		TL.seek(label);
		TL.play();
	}else if("to" == state){
		//TL.seek(label);
		TL.play(label);
	}else if("main" == state){
		if(label == ''){
			label = null;
		}
		
		var mcsArr = this.timelines['MC_TIMELINE'].obj;
		mcsArr.seek(label);
		
		var imgsArr = this.timelines['IMGS_TIMELINE'].obj;
		imgsArr.seek(label);
		
		var txtArr = this.timelines['TXT_TIMELINE'].obj;
		txtArr.seek(label);
		
	} else {
		TL.timeScale(1);
		TL[state]();
	}

	 
   
}


kas.Section.prototype.hasClass = function(el, name){
	return new RegExp('(\\s|^)'+name+'(\\s|$)').test(el.className);
}
kas.Section.prototype.addClass = function(el, name){
	if (!this.hasClass(el, name)) { el.className += (el.className ? ' ' : '') +name; }
}
kas.Section.prototype.loadDone = function(loadData) {

	
	//alert("loadDone" + loadData.blue[0]);
}

//GLOBAL IN AND OUTS
kas.Section.prototype.menuShowHide = function(menuActive) {
	/*console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  startSection  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + this.section_mc.length);*/
	if(menuActive){
		if(this.menu != true){
			this.menu = true;
			(this.section_menu.length) ? this.transitionIn("MENU_TIMELINE", "left") : '';
		}
		
	} else {
		if(this.menu != false){
			this.menu = false;
			(this.section_menu.length) ? this.transitionOut("MENU_TIMELINE", "left") : '';
		}
	}
/*
	(_section_xml.content.imgs.img.length()) ? transitionIn(section_img, _direction) : '';
	(_section_xml.content.customs.custom.length()) ? transitionIn(section_custom, _direction) : '';
	(_section_xml.content.navs.nav.length()) ? transitionIn(section_nav, _direction) : '';
	(_section_xml.content.txts.txt.length()) ? transitionIn(section_txt, _direction) : '';
*/
} 
/**
 * Start Section
 *
 * @return
 */
kas.Section.prototype.startSection = function() {

	
	(this.section_mc.length) ? this.transitionIn('MC_TIMELINE', "left") : '';
	(this.section_img.length) ? this.transitionIn('IMGS_TIMELINE', "left") : '';
	(this.section_txt.length) ? this.transitionIn('TXT_TIMELINE', "left") : '';
	this.state  	= 'open';
	
/*
	this.timelines['photoLines'].obj.seek(0);
		this.timelines['photoLines'].obj.stop();
*/
}
/**
 * Start Section
 *
 * @return
 */
kas.Section.prototype.closeSection = function() {
	if(typeof(this.activeTL) == 'object'){
		this.activeTL.duration = 0.5;
		//this.activeTL.timeScale(2.5);
		this.activeTL.reverse();
		this.activeTL = null;
	}
	for(var i = 0; i < this.animationTL.length; i++) { 
		var TLTemp = this.animationTL[i];
		TLTemp.seek(0);
		TLTemp.stop();
   }
   
   
/*
   	for (var prop in this.timelines) {
		this.timelines[prop].obj.seek(0);
		this.timelines[prop].obj.stop();
   }
*/
   (this.section_mc.length) ? this.transitionOut('MC_TIMELINE', "left") : '';
	(this.section_img.length) ? this.transitionOut('IMGS_TIMELINE', "left") : '';
	(this.section_txt.length) ? this.transitionOut('TXT_TIMELINE', "left") : '';
	this.state  	= 'close';
} 
/**
 * Transition Section In
 *
 * @return
 */
kas.Section.prototype.transitionIn = function(_items, _direction) {
	//alert(_items  + '  timeline  >>>>>  ' + this.timelines[_items].obj);
	
	//console.log("_items = "+_items);
	var TL = this.timelines[_items].obj;
	TL.timeScale(1);
	TL.play();
	///*console.log("_items.length = "+_items.length);*/
/*
	for(var i = 0; i < _items.length; i++) { 
		
		this.buildAnimation(_items[i], _items[i].params);
	}
*/
	
}//End TransitionIn
/**
 * Transition Section Out
 *
 * @return
 */
kas.Section.prototype.transitionOut = function(_items, _direction) {
	
	//trace("transitionOut _direction = "+_direction)
	//this.timelines[_items].obj.duration = 0.5;
	this.timelines[_items].obj.timeScale(5);
	this.timelines[_items].obj.reverse();
	
/*
	var i;
	for(i = 0; i < _items.length; i++) { 
	
		TweenLite.to(_items[i], _items[i].params.duration/2, {
			autoAlpha:_items[i].params.startAlpha, 
			left:_items[i].params.startX, 
			top:_items[i].params.startY,
			scale:_items[i].params.startScale,
			overwrite:1, 
			delay:(_items[i].params.delay/5),
			ease:_items[i].params.ease
		});
	}
*/
}
