# KASection

This component was made to develop single page animated application all using json. Orginal built for flash and converted over into Javascript.

## Example

To run the examples you can either upload 'Examples' folder to your server or run it locally by using the npm library inside the 'Examples' folder.

[View Example Online](https://www.klipartstudio.com/site/KAS_Sections/)

[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it

* Install the package from npm

`npm install`

* Start the local server

`npm start`

## Usage

* [Greensock](https://greensock.com) required

### DESCRIPTION:

* name : Optional - Name reference mostly used to jump between sections
* target : Div element you would like to target
* file : The Json file with all the params
* onComplete : Optional - Call back function oncee it is complete

Javascript code:

```javascript
// Loads the initial JSON file stage1.json
var stage1 = new kas.Section();

// Add PARAMS to Main Section
stage1.Tween(this, {
    name: "IntroScreen"
  target: "section",
  file: "stage1.json",
  onComplete: "finished"
});

// Get called once the sectiomn is loaded
function finished(event) {
    stage1.startSection();
}
```

HTML code:

```html
<div id='section'></div>
```


Json code:

```json
{
    "sections": [{
        "id": "main",   //Name reference mostly used to jump between sections
        "content": [{
            "mcs": [{   //Builds a div with all the items in it images or txt
                "imgs": [{}],   //Builds image objects
                "txts": [{}]    //Builds div to contain text
            }],
            "imgs": [{}],   //Builds image objects
            "txts": [{}],   //Builds div to contain text
            "timeline": [{  //Builds a sequence of animation and allows you to play them
                "id": "", //Name of the animation sequence for targeting 
                "seq": [{   //Builds a sequence of animation and allows you to play them
                    "on": [{}], //Handles rollover img or txt
                    "off": [{}], //Handles rollover img or txt
                    "open": [{}] //Add sub animation
                }]
            }]
        }]
    }]
}
```

## Parameters that can be used

### mcs, imgs, txts can all use these parameters

* id, top, left, x, y, autoAlpha, rotation, rotationX, rotationY, scale, scaleX, scaleY, skewX, skewY, background, color, width, height, overwrite, delay, ease, display, zindex, borderRadius, boxShadow, borders, padding, onComplete : Function call back, startLeft, startTop, startAlpha  , startRotation , startRotationX , startRotationY , startScale , startScaleX , startScaleY , startSkewX , startSkewY , startBackgroundColor  , startColor , startWidth  , startHeight  , startDisplay  , startZIndex , startBoxShadow 

### txts only parameters

* fontSize
* fontWeight
* fontAlign
* lineHeight

### timeline only parameters

* timePosition : default is 0, using a number will add item absolute place in time or use '+=Amount of seconds' or '-=Amount of seconds' will add the element after the previous element the amount of seconds you specify
* staggerAmt : default is 0, if number is added here it will seperate the stagger of the elements
* itemType : Is required for the items added

### Active Click

* active":"ad_photo_start",
* jump : Function to call after click
* timeline : Timeline 'id' name you want to target
* state : Target sub animation in a timeline
* label : Timelines allow labels so it is easy to jump to different place in the timeline

### Medthods Availible

* startSection
* closeSection

### Available options for animation

* stage.timeCall('timeline Name', 'jump', (INT seconds));
* stage.timeCall('timeline Name', 'reverse', '');
* stage.timeCall('timeline Name', 'hide', '');
* stage.timeCall('timeline Name', 'to', 'label name');
* stage.timeCall('timeline Name', 'play', '');

## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASection)

## License

MIT
